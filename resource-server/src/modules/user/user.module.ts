import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../core/database/database.module';
import { UserMongooseProvider } from './repository/user.mongoose.provider';
import { UserRepository } from './repository/user.repository';
import { AuthorizationModule } from '../authorization/authorization.module';
import { ChatModule } from '../chat/chat.module';
import { UserGateway } from './user.gateway';
import { UserController } from './user.controller';

@Module({
  imports: [DatabaseModule, AuthorizationModule, ChatModule],
  providers: [UserRepository, UserMongooseProvider, UserGateway],
  controllers: [UserController],
  exports: [UserRepository],
})
export class UserModule {}
