import { Connection } from 'mongoose';
import { userSchema } from './user.schema';

export const UserMongooseProvider = {
  provide: 'USER_MODEL',
  useFactory: (connection: Connection) => connection.model('users', userSchema),
  inject: ['DATABASE_CONNECTION'],
};
