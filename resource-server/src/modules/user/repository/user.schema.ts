import * as mongoose from 'mongoose';

export const userSchema = new mongoose.Schema(
  {
    username: String,
    publicKey: String,
  },
  { timestamps: true },
);
