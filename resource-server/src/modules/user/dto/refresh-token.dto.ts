import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class RefreshTokenrDto {
  @IsString()
  @Transform(value => String(value))
  public readonly authToken: string;

  @IsString()
  @Transform(value => String(value))
  public readonly refreshToken: string;
}
