import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class StartChatDto {
  @IsString()
  @Transform(value => String(value))
  public readonly username: string;
}
