import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class StartChatBodyDto {
  @IsString()
  @Transform(value => String(value))
  public readonly authToken: string;

  @IsString()
  @Transform(value => String(value))
  public readonly refreshToken: string;

  @IsString()
  @Transform(value => String(value))
  public readonly encryptedSessionKey: string;
}
