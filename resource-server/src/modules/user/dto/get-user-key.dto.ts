import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetUserKeyDto {
  @IsString()
  @Transform(value => String(value))
  public readonly username: string;
}
