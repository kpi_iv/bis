import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class SendMessageDto {
  @IsString()
  @Transform(value => String(value))
  public readonly chatId: string;

  @IsString()
  @Transform(value => String(value))
  public readonly message: string;
}
