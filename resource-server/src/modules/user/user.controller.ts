import { Controller, UseInterceptors, ClassSerializerInterceptor, Post, Body, Query, NotFoundException } from '@nestjs/common';
import { RefreshTokenrDto, SendMessageDto, GetUserKeyDto, StartChatDto, StartChatBodyDto } from './dto';
import { AuthorizationService } from '../authorization/services/authorization.service';
import { ChatRepository } from '../chat/repository/chat.repository';
import { UserRepository } from './repository/user.repository';
import { ChatsSerializer } from '../chat/serializers/chats.serializer';

@Controller()
export class UserController {
  constructor(
    private readonly authorizationService: AuthorizationService,
    private readonly chatRepository: ChatRepository,
    private readonly userRepository: UserRepository,
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('key')
  async getKey(@Body() dto: RefreshTokenrDto, @Query() params: GetUserKeyDto) {
    this.authorizationService.validateToken(dto.authToken);

    const { username } = params;
    const user = await this.userRepository.findOne({ username });
    if (!user) throw new NotFoundException('User not found!');

    return { publicKey: user.publicKey };
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('send')
  async send(@Body() dto: RefreshTokenrDto, @Query() params: SendMessageDto) {
    this.authorizationService.validateToken(dto.authToken);

    const { chatId, message } = params;
    const { aud } = this.authorizationService.getPayload(dto.authToken);
    const senderId = aud;

    const chat = await this.chatRepository.findOne({ _id: chatId });
    if (!chat) throw new NotFoundException('Chat not found!');

    chat.messages.push({ sender: senderId, message });
    await this.chatRepository.update(chat._id, chat);
    return 'Message send';
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('start')
  async create(@Body() dto: StartChatBodyDto, @Query() params: StartChatDto) {
    this.authorizationService.validateToken(dto.authToken);

    const { encryptedSessionKey } = dto;
    const { username } = params;
    const recipient = await this.userRepository.findOne({ username });
    if (!recipient) throw new NotFoundException('Recipient not found!');

    const { aud } = this.authorizationService.getPayload(dto.authToken);
    const senderId = aud;

    const chat = await this.chatRepository.findOne({ users: [recipient._id, senderId] });
    if (chat) throw new NotFoundException('Chat exists!');

    const newChat = {
      users: [recipient._id, senderId],
      messages: [],
      encryptedSessionKey,
    };
    await this.chatRepository.create(newChat);
    return 'Chat created!';
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('chats')
  async getChats(@Body() dto: RefreshTokenrDto) {
    this.authorizationService.validateToken(dto.authToken);

    const { aud } = this.authorizationService.getPayload(dto.authToken);
    const senderId = aud;

    const chats = await this.chatRepository.find({ users: { $all: [senderId] } });
    // return chats.map(chat => new ChatsSerializer(chat));
    return chats;
  }
}
