import { Document } from 'mongoose';

export interface User {
  _id: any;
  username: string;
  publicKey?: string;
}

export interface MongooseUser extends User, Document {}
