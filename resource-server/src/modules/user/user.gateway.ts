import { WebSocketGateway, SubscribeMessage, MessageBody, WsException } from '@nestjs/websockets';
import { AuthorizationService } from '../authorization/services/authorization.service';

@WebSocketGateway(5001, { namespace: 'users' })
export class UserGateway {
  constructor(private readonly authorizationService: AuthorizationService) {}

  // @SubscribeMessage('send')
  // async onMessage(@MessageBody() data: any) {
  // }
}
