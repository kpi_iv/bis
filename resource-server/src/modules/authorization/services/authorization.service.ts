import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../shared/config/constants';
import { TokenService } from './token.service';

@Injectable()
export class AuthorizationService {
  constructor(private readonly configService: ConfigService, private readonly tokenService: TokenService) {}

  validateToken(token: string): boolean {
    const AUTH_SERVER_PUBLIC_KEY = this.configService.get(ENV_VARIABLE_KEY.AUTH_SERVER_PUBLIC_KEY);
    return this.tokenService.validate(token, AUTH_SERVER_PUBLIC_KEY);
  }

  getPayload(token: string): any {
    let [, payload]: any = token.split('.');
    return this.tokenService.base64ToObject(payload);
  }
}
