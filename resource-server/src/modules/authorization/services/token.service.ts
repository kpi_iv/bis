import * as RSA from 'node-rsa';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../shared/config/constants';
import { WsException } from '@nestjs/websockets';

@Injectable()
export class TokenService {
  constructor(private readonly configService: ConfigService) {}

  validate(token: string, key: any): boolean {
    const ISSUER_ID = this.configService.get(ENV_VARIABLE_KEY.ISSUER_ID);
    let [header, payload, signature]: any = token.split('.');

    key = new RSA(key);
    if (!key.verify(`${header}.${payload}`, Buffer.from(signature, 'base64'))) throw new UnauthorizedException('Signature is Invalid!');

    payload = this.base64ToObject(payload);
    if (payload.iss !== ISSUER_ID) throw new UnauthorizedException('Im not an issuer!');

    const nowInSec = Math.round(new Date().getTime() / 1000);
    if (payload.exp < nowInSec) throw new UnauthorizedException('The token is expired!');
    return true;
  }

  base64ToObject(data: string): string {
    return JSON.parse(Buffer.from(data, 'base64').toString('utf8'));
  }
}
