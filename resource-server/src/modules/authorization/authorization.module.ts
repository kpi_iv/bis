import { Module } from '@nestjs/common';
import { AuthorizationService } from './services/authorization.service';
import { TokenService } from './services/token.service';

@Module({
  imports: [],
  controllers: [],
  providers: [AuthorizationService, TokenService],
  exports: [AuthorizationService],
})
export class AuthorizationModule {}
