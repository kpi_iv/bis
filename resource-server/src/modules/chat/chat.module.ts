import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../core/database/database.module';
import { ChatMongooseProvider } from './repository/chat.mongoose.provider';
import { ChatRepository } from './repository/chat.repository';

@Module({
  imports: [DatabaseModule],
  providers: [ChatRepository, ChatMongooseProvider],
  exports: [ChatRepository],
})
export class ChatModule {}
