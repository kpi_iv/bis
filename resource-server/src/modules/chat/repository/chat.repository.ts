import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { MongooseChat, Chat } from '../interfaces';

@Injectable()
export class ChatRepository {
  constructor(
    @Inject('CHAT_MODEL')
    private readonly chatModel: Model<MongooseChat>,
  ) {}

  async create(userToCreate: Partial<Chat>): Promise<unknown> {
    const createdUser = new this.chatModel(userToCreate);
    return createdUser.save();
  }

  async save(user: Chat): Promise<unknown> {
    return this.chatModel.findOneAndUpdate({ user_id: user._id }, user, { upsert: true });
  }

  async findOne(query: object): Promise<Chat> {
    const user = await this.chatModel.findOne(query);
    return !user ? user : user.toObject();
  }

  async find(query: object): Promise<Chat[]> {
    const users = await this.chatModel.find(query);
    return users.map(u => u.toObject());
  }

  async update(_id: string, query: object): Promise<unknown> {
    return this.chatModel.updateOne({ _id }, query);
  }
}
