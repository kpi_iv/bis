import { Connection } from 'mongoose';
import { chatsSchema } from './chat.schema';

export const ChatMongooseProvider = {
  provide: 'CHAT_MODEL',
  useFactory: (connection: Connection) => connection.model('chats', chatsSchema),
  inject: ['DATABASE_CONNECTION'],
};
