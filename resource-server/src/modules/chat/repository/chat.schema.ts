import * as mongoose from 'mongoose';

export const chatsSchema = new mongoose.Schema(
  {
    users: [String],
    messages: [Object],
    encryptedSessionKey: String, // asymmetric session key encrypted with recipient public key
  },
  { timestamps: true },
);
