import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ChatsSerializer {
  @Expose()
  _id: string;

  @Expose()
  users: string[];

  @Expose()
  messages: object[];

  @Expose()
  encryptedSessionKey: string;

  constructor(partial: Partial<ChatsSerializer>) {
    Object.assign(this, partial);
  }
}
