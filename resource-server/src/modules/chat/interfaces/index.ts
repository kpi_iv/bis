import { Document } from 'mongoose';

export interface Chat {
  _id: any;
  users: string[];
  messages: object[];
  encryptedSessionKey: string;
}

export interface MongooseChat extends Chat, Document {}
