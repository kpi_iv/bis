import { Module } from '@nestjs/common';
import { CoreModule } from './core/core.module';
import { sharedModules } from './shared/shared.module';
import { UserModule } from './modules/user/user.module';
import { AuthorizationModule } from './modules/authorization/authorization.module';
import { UserController } from './modules/user/user.controller';
import { ChatModule } from './modules/chat/chat.module';

@Module({
  imports: [...sharedModules, CoreModule, AuthorizationModule, UserModule, ChatModule],
  controllers: [UserController],
})
export class AppModule {}
