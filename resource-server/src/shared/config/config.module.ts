import { Module, Global } from '@nestjs/common';
import { ConfigService } from './services';

process.env.NODE_ENV = 'dev';
@Global()
@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV}.env`),
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
