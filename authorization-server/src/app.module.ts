import { Module } from '@nestjs/common';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { CoreModule } from './core/core.module';
import { sharedModules } from './shared/shared.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [...sharedModules, CoreModule, AuthenticationModule, AuthenticationModule, UserModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
