import * as mongoose from 'mongoose';

export const userSchema = new mongoose.Schema(
  {
    username: String,
    password: String,
    refreshToken: String,
    publicKey: String,
  },
  { timestamps: true },
);
