import { Injectable, Inject } from '@nestjs/common';
import { MongooseUser, User } from '../interfaces';
import { Model } from 'mongoose';

@Injectable()
export class UserRepository {
  constructor(
    @Inject('USER_MODEL')
    private readonly userModel: Model<MongooseUser>,
  ) {}

  async create(userToCreate: Partial<User>): Promise<unknown> {
    const createdUser = new this.userModel(userToCreate);
    return createdUser.save();
  }

  async save(user: User): Promise<unknown> {
    return this.userModel.findOneAndUpdate({ user_id: user._id }, user, { upsert: true });
  }

  async findOne(query: object): Promise<User> {
    const user = await this.userModel.findOne(query);
    return !user ? user : user.toObject();
  }

  async find(query: object): Promise<User[]> {
    const users = await this.userModel.find(query);
    return users.map(u => u.toObject());
  }

  async update(_id: string, query: object): Promise<unknown> {
    return this.userModel.updateOne({ _id }, query);
  }
}
