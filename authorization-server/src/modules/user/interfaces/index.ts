import { Document } from 'mongoose';

export interface User {
  _id: any;
  username: string;
  password: string;
  refreshToken: string;
  publicKey?: string;
}

export interface MongooseUser extends User, Document {}
