import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../core/database/database.module';
import { UserMongooseProvider } from './repository/user.mongoose.provider';
import { UserRepository } from './repository/user.repository';

@Module({
  imports: [DatabaseModule],
  providers: [UserRepository, UserMongooseProvider],
  exports: [UserRepository],
})
export class UserModule {}
