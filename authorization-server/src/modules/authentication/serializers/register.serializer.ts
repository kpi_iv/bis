import { Exclude, Transform, Expose } from 'class-transformer';

@Exclude()
export class RegisterSerializer {
  @Expose()
  username: string;

  @Expose()
  password: string;

  constructor(partial: Partial<RegisterSerializer>) {
    Object.assign(this, partial);
  }
}
