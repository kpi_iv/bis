import { Controller, UseInterceptors, ClassSerializerInterceptor, Post, Body } from '@nestjs/common';
import { RegisterUserDto, LoginUserDto, RefreshTokenrDto } from './dto';
import { AuthenticationService } from './services/authentication.service';
import { AuthorizationService } from '../authorization/services/authorization.service';

@Controller()
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService, private authorizationService: AuthorizationService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('register')
  async register(@Body() dto: RegisterUserDto) {
    await this.authenticationService.registerUser(dto);
    return 'Successfully registered user';
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('login')
  async login(@Body() dto: LoginUserDto) {
    const user = await this.authenticationService.authenticateUser(dto.username, dto.password);
    const tokens = this.authorizationService.createTokenPair(user);
    await this.authorizationService.updateUserToken(user._id, tokens.refreshToken);
    return tokens;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('refresh')
  async refresh(@Body() dto: RefreshTokenrDto) {
    this.authorizationService.validateRefreshToken(dto.refreshToken);
    const tokens = this.authorizationService.refreshTokenPair(dto.authToken, dto.refreshToken);
    const payload = this.authorizationService.getPayload(tokens.authToken);
    await this.authorizationService.updateUserToken(payload._id, tokens.refreshToken);
    return tokens;
  }
}
