import * as bcrypt from 'bcrypt';
import { Injectable, ConflictException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from '../../user/repository/user.repository';
import { ConfigService } from '../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../shared/config/constants';
import { User } from '../../user/interfaces';
import { RegisterUserDto } from '../dto';

@Injectable()
export class AuthenticationService {
  constructor(private readonly userRepository: UserRepository, private readonly configService: ConfigService) {}

  async registerUser(userDto: RegisterUserDto): Promise<any> {
    const { username, password, publicKey } = userDto;
    const user = await this.userRepository.findOne({ username });
    if (user) throw new ConflictException('User exists!');

    const SALT_ROUNDS = this.configService.get(ENV_VARIABLE_KEY.SALT_ROUNDS);
    const LOCAL_SALT = this.configService.get(ENV_VARIABLE_KEY.LOCAL_SALT);

    const salt = bcrypt.genSaltSync(Number(SALT_ROUNDS));
    const passwordHashed = bcrypt.hashSync(password + LOCAL_SALT, salt);
    const newUser = {
      username,
      password: passwordHashed,
      publicKey,
    };
    return this.userRepository.create(newUser);
  }

  async authenticateUser(username: string, plainPassword: string): Promise<any> {
    const user = await this.userRepository.findOne({ username });
    if (!user) throw new NotFoundException('User do not exist!');

    const LOCAL_SALT = this.configService.get(ENV_VARIABLE_KEY.LOCAL_SALT);
    const authenticated = await bcrypt.compare(plainPassword + LOCAL_SALT, user.password);
    if (!authenticated) throw new UnauthorizedException('Password is invalid!');

    const { password, ...userDto } = user;
    return userDto;
  }
}
