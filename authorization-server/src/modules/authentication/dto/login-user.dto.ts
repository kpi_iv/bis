import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class LoginUserDto {
  @IsString()
  @Transform(value => String(value))
  public readonly username: string;

  @IsString()
  @Transform(value => String(value))
  public readonly password: string;
}
