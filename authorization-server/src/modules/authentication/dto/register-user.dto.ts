import { IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class RegisterUserDto {
  @IsString()
  @Transform(value => String(value))
  public readonly username: string;

  @IsString()
  @Transform(value => String(value))
  public readonly password: string;

  @IsString()
  @Transform(value => String(value))
  public readonly publicKey: string;
}
