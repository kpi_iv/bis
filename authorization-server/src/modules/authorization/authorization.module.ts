import { Module } from '@nestjs/common';
import { AuthorizationService } from './services/authorization.service';
import { TokenService } from './services/token.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [UserModule],
  controllers: [],
  providers: [AuthorizationService, TokenService],
  exports: [AuthorizationService],
})
export class AuthorizationModule {}
