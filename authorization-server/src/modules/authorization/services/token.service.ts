import * as uuid from 'uuid/v1';
import * as RSA from 'node-rsa';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../shared/config/constants';

@Injectable()
export class TokenService {
  constructor(private readonly configService: ConfigService) {}

  // * aud should be presented in the payload
  // * no sensitive data the payload
  sign(payload: any, ttl: number, key: any): string {
    const ISSUER_ID = this.configService.get(ENV_VARIABLE_KEY.ISSUER_ID);
    const nowInSec = Math.round(new Date().getTime() / 1000);

    payload = this.objectToBase64({
      iss: ISSUER_ID,
      sub: 'auth',
      exp: nowInSec + ttl,
      nbf: nowInSec,
      iat: nowInSec,
      jti: uuid(),
      ...payload,
    });

    const header = this.objectToBase64({
      alg: 'RSA',
      typ: 'JWT',
    });

    key = new RSA(key);
    const signature = key.sign(`${header}.${payload}`, 'base64');
    const token = `${header}.${payload}.${signature}`;
    return token;
  }

  validate(token: string, key: any): boolean {
    const ISSUER_ID = this.configService.get(ENV_VARIABLE_KEY.ISSUER_ID);
    let [header, payload, signature]: any = token.split('.');

    key = new RSA(key);
    if (!key.verify(`${header}.${payload}`, Buffer.from(signature, 'base64'))) throw new UnauthorizedException('Signature is Invalid!');

    payload = this.base64ToObject(payload);
    if (payload.iss !== ISSUER_ID) throw new UnauthorizedException('Im not an issuer!');

    const nowInSec = Math.round(new Date().getTime() / 1000);
    if (payload.exp < nowInSec) throw new UnauthorizedException('The token is expired!');
    return true;
  }

  private objectToBase64(data: object): string {
    return Buffer.from(JSON.stringify(data)).toString('base64');
  }

  base64ToObject(data: string): string {
    return JSON.parse(Buffer.from(data, 'base64').toString('utf8'));
  }
}
