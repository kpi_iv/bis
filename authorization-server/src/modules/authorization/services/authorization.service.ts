import { Injectable } from '@nestjs/common';
import { ConfigService } from '../../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../../shared/config/constants';
import { User } from '../../user/interfaces';
import { TokenService } from './token.service';
import { UserRepository } from '../../user/repository/user.repository';

@Injectable()
export class AuthorizationService {
  constructor(
    private readonly configService: ConfigService,
    private readonly tokenService: TokenService,
    private readonly userRepository: UserRepository,
  ) {}

  createTokenPair(user: Partial<User>): any {
    const JWT_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_SECRET);
    const JWT_TTL = this.configService.get(ENV_VARIABLE_KEY.JWT_TTL);
    const JWT_REFRESH_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_REFRESH_SECRET);
    const JWT_REFRESH_TTL = this.configService.get(ENV_VARIABLE_KEY.JWT_REFRESH_TTL);

    const refreshToken = this.tokenService.sign({ aud: user._id, role: 'refresh' }, Number(JWT_REFRESH_TTL), JWT_REFRESH_SECRET);
    const [, signatur] = refreshToken.split('.');
    const authToken = this.tokenService.sign({ aud: user._id, role: 'client', refreshToken: signatur }, Number(JWT_TTL), JWT_SECRET);

    return { authToken, refreshToken };
  }

  refreshTokenPair(authToken: any, refreshToken: string) {
    const JWT_REFRESH_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_REFRESH_SECRET);
    this.tokenService.validate(refreshToken, JWT_REFRESH_SECRET);

    const payload = this.getPayload(authToken);
    const JWT_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_SECRET);
    const JWT_TTL = this.configService.get(ENV_VARIABLE_KEY.JWT_TTL);

    const newAuthToken = this.tokenService.sign({ aud: payload.aud, role: 'client', refreshToken }, Number(JWT_TTL), JWT_SECRET);
    return { authToken: newAuthToken, refreshToken };
  }

  validateToken(token: string) {
    const JWT_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_SECRET);
    return this.tokenService.validate(token, JWT_SECRET);
  }

  validateRefreshToken(token: string) {
    const JWT_REFRESH_SECRET = this.configService.get(ENV_VARIABLE_KEY.JWT_REFRESH_SECRET);
    return this.tokenService.validate(token, JWT_REFRESH_SECRET);
  }

  getPayload(token: string): any {
    let [, payload]: any = token.split('.');
    return this.tokenService.base64ToObject(payload);
  }

  async updateUserToken(user_id: string, token: string): Promise<any> {
    return this.userRepository.update(user_id, { refreshToken: token });
  }

  async revokeUserToken(user_id: string): Promise<any> {
    return this.userRepository.update(user_id, { refreshToken: 'revoked' });
  }
}
