as:
	cd ./authorization-server && npm run start:dev
rs:
	cd ./resource-server && npm run start:dev
c:
	cd ./client && node src/index.js
i:
	cd ./client && npm i && cd ../resource-server && npm i && cd ../authorization-server && npm i