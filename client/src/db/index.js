const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

module.exports.launch = function() {
  const adapter = new FileSync('src/db/db.json');
  const db = low(adapter);
  db.defaults({ user: {} }).write();
  return db;
};
