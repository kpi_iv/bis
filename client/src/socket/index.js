const io = require('socket.io-client');

module.exports = async () => {
  return new Promise(resolve => {
    const socket = io('http://localhost:5001/users');
    socket.on('connect_error', function() {
      console.log('Connection Failed');
    });

    socket.on('connect', function(data) {
      console.log('Connected');
      resolve(socket);
    });

    socket.on('disconnect', function() {
      console.log('Disconnected');
    });
  });
};
