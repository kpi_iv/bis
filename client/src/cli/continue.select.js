const prompts = require('prompts');

module.exports = async () => {
  const response = await prompts([
    {
      type: 'toggle',
      name: 'value',
      message: 'Do you want to continue?',
      initial: true,
      active: 'yes',
      inactive: 'no',
    },
  ]);
  return response.value;
};
