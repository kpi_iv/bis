const prompts = require('prompts');
const SHA256 = require('crypto-js/sha256');

module.exports = async () => {
  const { username } = await prompts({
    type: 'text',
    name: 'username',
    message: 'Insert recipient username:',
    validate: value => (value.length == 0 ? `Username.length > 0` : true),
  });

  const { message } = await prompts({
    type: 'text',
    name: 'message',
    message: 'Insert message:',
    validate: value => (value.length == 0 ? `Username.length > 0` : true),
  });
  return { username: String(username), message: String(message) };
};
