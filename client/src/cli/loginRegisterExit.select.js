const prompts = require('prompts');

module.exports = async () => {
  const response = await prompts([
    {
      type: 'select',
      name: 'select',
      message: 'What do you want to do?',
      choices: [{ title: 'login', value: 'login' }, { title: 'register', value: 'register' }, { title: 'exit', value: 'exit' }],
    },
  ]);
  return response.select;
};
