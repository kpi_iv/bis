const prompts = require('prompts');

module.exports = async () => {
  const response = await prompts([
    {
      type: 'select',
      name: 'select',
      message: 'What do you want to do?',
      choices: [{ title: 'send', value: 'send' }, { title: 'logout', value: 'logout' }, { title: 'exit', value: 'exit' }],
    },
  ]);
  if (response.select === 'exit') process.exit();
  return response.select;
};
