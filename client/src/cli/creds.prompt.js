const prompts = require('prompts');
const SHA256 = require('crypto-js/sha256');

module.exports = async () => {
  const { username } = await prompts({
    type: 'text',
    name: 'username',
    message: 'Insert username:',
    validate: value => (value.length == 0 ? `Username.length > 0` : true),
  });

  const { password } = await prompts({
    type: 'password',
    name: 'password',
    message: 'Insert password:',
    validate: value => (value.length < 6 ? `Password.length >= 6` : true),
  });
  return { username, password: SHA256(password) };
};
