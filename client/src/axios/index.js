const axios = require('axios');

module.exports.instanceAS = axios.create({
  baseURL: 'http://localhost:4000/api/v1/',
  timeout: 20000,
});

module.exports.instanceRS = axios.create({
  baseURL: 'http://localhost:5000/api/v1/',
  timeout: 20000,
});
