'use strict';

const { instanceAS } = require('../../axios');
const credentialsPrompt = require('../../cli/creds.prompt');
const continueSelect = require('../../cli/continue.select');

module.exports = async db => {
  while (true) {
    const creds = await credentialsPrompt();

    try {
      const { data } = await instanceAS.post('login', creds);
      db.set('user.tokens', data).write();
      console.log('Login done!');
      return true;
    } catch (e) {
      if (e.response && e.response.data) console.log(e.response.data);
      else console.log(e);
    }
    if (!(await continueSelect())) return false;
  }
};
