const register = require('./register.service');
const login = require('./login.service');

const loginRegisterExitSelect = require('../../cli/loginRegisterExit.select');

module.exports = async db => {
  let temp = false;
  while (!temp) {
    const action = await loginRegisterExitSelect();
    if (action === 'register') temp = await register(db);
    else if (action === 'login') temp = await login(db);
    else process.exit();
  }
  return db.get('user.tokens').value();
};
