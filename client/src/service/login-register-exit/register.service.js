'use strict';

const RSA = require('node-rsa');
const { instanceAS } = require('../../axios');
const credentialsPrompt = require('../../cli/creds.prompt');
const continueSelect = require('../../cli/continue.select');

module.exports = async db => {
  const key = new RSA({ b: 512 });
  const publicKey = key.exportKey('public');
  const privateKey = key.exportKey();
  db.set('user.keys', { privateKey }).write();

  while (true) {
    const creds = await credentialsPrompt();
    const registerPayload = {
      publicKey,
      ...creds,
    };

    try {
      await instanceAS.post('register', registerPayload);
      const { data } = await instanceAS.post('login', creds);
      db.set('user.tokens', data).write();
      console.log('Registration done!');
      return true;
    } catch (e) {
      if (e.response && e.response.data) console.log(e.response.data);
      else console.log(e);
    }
    if (!(await continueSelect())) return false;
  }
};
