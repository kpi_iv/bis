'use strict';

const RSA = require('node-rsa');
const crypto = require('crypto');
const { instanceRS } = require('../../../axios');

const startChat = async (db, tokens, username) => {
  console.log('Trying start chat');
  const publicKey = await getUserKey(username, tokens);

  //TODO: generate for every chat
  const sessionKey = 'sessionKey';

  const recPublicKey = RSA(publicKey);
  const encryptedSessionKey = recPublicKey.encrypt(sessionKey, 'base64');

  let { privateKey } = db.get('user.keys').value();
  const senderPrivateKey = RSA(privateKey);
  const signature = senderPrivateKey.sign(encryptedSessionKey, 'base64');

  const transaction = `${encryptedSessionKey}:${signature}`;
  const { data } = await instanceRS.post(`start?username=${username}&sessionKey=${transaction}`, tokens);
  process.exit();
};

const getUserKey = async (username, tokens) => {
  try {
    const { data } = await instanceRS.post(`key?username=${username}`, tokens);
    return data.publicKey;
  } catch (e) {
    console.log('Failde to create chat!'.e);
    process.exit();
  }
};

module.exports = startChat;
