'use strict';

const { instanceAS } = require('../../../axios');

module.exports = async db => {
  try {
    let tokens = db.get('user.tokens').value();
    const { data } = await instanceAS.post('refresh', tokens);
    db.set('user.tokens', data).write();
    console.log('Token refreshed');
    return data;
  } catch (e) {
    //TODO check refresh token invalid case
    console.log(e);
    process.exit();
  }
};
