'use strict';

module.exports = async db => {
  const user = db.get('user').value();
  const { tokens, ...newUser } = user;
  db.set('user', newUser).write();
};
