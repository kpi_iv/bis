'use strict';

const sendMessageSelect = require('../../cli/sendMessage.select');
const sendMessagePrompt = require('../../cli/send.message.prompt');

const logout = require('./logout.service');
const sendMessageService = require('./send-message.service');

const chooseAction = async (db, tokens) => {
  let temp = false;
  while (!temp) {
    const action = await sendMessageSelect();
    if (action === 'logout') {
      await logout(db);
      process.exit();
    } else if (action === 'exit') {
      process.exit();
    } else if (action === 'send') {
      const { username, message } = await sendMessagePrompt();
      temp = await sendMessageService(db, tokens, username, message);
    } else throw new Error('Something bad happens');
  }
  console.log('Finished somehow');
};

module.exports = chooseAction;
