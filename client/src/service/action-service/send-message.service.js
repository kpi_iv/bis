'use strict';

const { instanceRS } = require('../../axios');

const refreshToken = require('./helpers/refresh-token.service');
const startChatService = require('./helpers/start-chat.service');

const sendMessage = async (db, tokens, username, message) => {
  try {
    await instanceRS.post(`send?username=${username}&message=${message}`, tokens);
  } catch (e) {
    if (e.response && e.response.data) {
      if (e.response.data.message === 'The token is expired!') {
        await refreshToken(db);
        await sendMessage(db, tokens, username, message);
        return true;
      } else if (e.response.data.message === 'User not found!') {
        console.log(e.response.data.message);
        return false;
      } else if (e.response.data.message === 'Chat not found!') {
        await startChatService(db, tokens, username);
        await sendMessage(db, tokens, username, message);
        return true;
      } else {
        console.log(e.response);
        process.exit();
      }
    } else {
      console.log(e);
      process.exit();
    }
  }
};

module.exports = sendMessage;
