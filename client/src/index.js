'use strict';

const low = require('./db');
const createConnection = require('./socket');

const loginRegisterExitService = require('./service/login-register-exit');
const actionService = require('./service/action-service');

(async () => {
  const db = low.launch();
  let tokens = db.get('user.tokens').value();

  tokens = tokens || (await loginRegisterExitService(db));
  // const socket = await createConnection(tokens.authToken);

  await actionService(db, tokens);
})();
