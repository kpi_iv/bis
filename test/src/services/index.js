const { instanceAS, instanceRS } = require('./axios');

async function registerUser(user) {
  console.log('Register User:');
  const { privateKey, ...registerPayload } = user;
  await instanceAS.post('register', registerPayload);
}

async function loginUser(user) {
  console.log('Login User:');
  const { privateKey, publicKey, ...loginPayload } = user;
  const { data } = await instanceAS.post('login', loginPayload);
  // console.log('Login User: user tokens: ' + data.authToken);
  return data;
}

async function getKey(username, tokens) {
  console.log('Get Key:');
  const { data } = await instanceRS.post(`key?username=${username}`, tokens);
  return data.publicKey;
}

async function startChat(username, encryptedSessionKey, tokens) {
  console.log('Start Chat:');
  tokensPayload = { ...tokens, encryptedSessionKey };
  await instanceRS.post(`start?username=${username}`, tokensPayload);
}

async function getChats(tokens) {
  console.log('Get Chat:');
  const { data } = await instanceRS.post(`chats`, tokens);
  return data;
}

async function sendMessage(chatID, message, tokens) {
  console.log('Send Message:');
  await instanceRS
    .post(`send?message=${message}&chatId=${chatID}`, tokens)
    .then()
    .catch(e => console.log(JSON.stringify(e.response.data.message)));
}

module.exports = {
  registerUser,
  loginUser,
  getKey,
  startChat,
  getChats,
  sendMessage,
};
