const mongoose = require('mongoose');

module.exports = async function() {
  await mongoose.connect('mongodb://localhost:27017/bis-as-dev', { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await mongoose.connection.db.dropCollection('users');
    await mongoose.connection.db.dropCollection('chats');
  } catch (e) {}

  mongoose.connection.close();
};
