const SHA256 = require('crypto-js/sha256');
const RSA = require('node-rsa');

const { key1PrivateKey, key1PublicKey, key2PrivateKey, key2PublicKey } = require('./keys');

function generateKeyPair() {
  const key = new RSA().generateKeyPair();
  const publicKey = key.exportKey('pkcs8-public-pem');
  const privateKey = key.exportKey('pkcs1-pem');
  console.log(publicKey, privateKey);
  return { publicKey, privateKey };
}

const user1 = {
  username: 'username1',
  password: SHA256('password1').toString(),
  // ...generateKeyPair(),
  publicKey: key1PublicKey,
  privateKey: key1PrivateKey,
};

const user2 = {
  username: 'username2',
  password: SHA256('password2').toString(),
  // ...generateKeyPair(),
  publicKey: key2PublicKey,
  privateKey: key2PrivateKey,
};

module.exports = {
  user1,
  user2,
};
