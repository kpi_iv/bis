'use strict';

const crypto = require('crypto');
const RSA = require('node-rsa');

const clearMongo = require('./scripts/removeAll');
const { user1, user2 } = require('./fixtures');
const { registerUser, loginUser, getKey, startChat, getChats, sendMessage } = require('./services');

(async () => {
  await clearMongo();

  await registerUser(user1);
  await registerUser(user2);

  const user1Tokens = await loginUser(user1);
  const user2Tokens = await loginUser(user2);

  const user2PublicKey = await getKey(user1.username, user1Tokens);

  // console.log(typeof user2PublicKey);
  // console.log(typeof user2.publicKey);
  // for (let i = 0; i < user2PublicKey.length; i++) {
  //   console.log(user2PublicKey[0], user2.publicKey[0], user2PublicKey[0] === user2.publicKey[0]);
  // }

  const sessionKey = 'ciw7p02f70000ysjon7gztjn70klskks';
  const encryptedSessionKey = getEncryptedSessionKey(sessionKey, user2.privateKey);

  await startChat(user2.username, encryptedSessionKey, user1Tokens);

  let chats = await getChats(user2Tokens);
  const _encryptedSessionKey = chats[0].encryptedSessionKey;
  const chatId = chats[0]._id;

  const _sessionKey = getDencryptedSessionKey(_encryptedSessionKey, user2.privateKey);
  console.log(_sessionKey);

  const message = symmetricEncryption('message', sessionKey);
  await sendMessage(chatId, message, user1Tokens);

  chats = await getChats(user2Tokens);
  console.log(
    chats[0].messages.map(m => {
      m.message = symmetricDecryption(m.message, _sessionKey);
      return m;
    }),
  );
})();

function getEncryptedSessionKey(sessionKey, publicKey) {
  console.log('Encrypte session Key:');
  const key = new RSA(publicKey);
  return key.encrypt(sessionKey).toString('binary');
}

function getDencryptedSessionKey(encryptedSessionKey, privateKey) {
  console.log('Decrypte session Key:');
  encryptedSessionKey = Buffer.from(encryptedSessionKey, 'binary');
  const key = new RSA(privateKey);
  return key.decrypt(encryptedSessionKey).toString('utf-8');
}

const IV_LENGTH = 16; // For AES, this is always 16;

function symmetricEncryption(text, ENCRYPTION_KEY) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
  let encrypted = cipher.update(text);

  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function symmetricDecryption(text, ENCRYPTION_KEY) {
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
  let decrypted = decipher.update(encryptedText);

  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted.toString();
}
