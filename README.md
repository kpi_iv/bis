## Description
Client-to-client communication through server with end to end encryption

## It's not production ready code, don't use it in pruduction!

## Links
https://habr.com/ru/company/acribia/blog/413157/

https://auth0.com/blog/hashing-in-action-understanding-bcrypt/

https://habr.com/ru/company/mailru/blog/343288/

https://habr.com/ru/company/mailru/blog/115163/

Nest security page
https://docs.nestjs.com/techniques/security

JWT & access abd refresh tokens
https://gist.github.com/zmts/802dc9c3510d79fd40f9dc38a12bccfc

Mail ru security tips
https://habr.com/ru/company/mailru/blog/228997/

XSS
https://habr.com/ru/post/66057/?var=%3Cscript%3Ealert(%27xss%27);%3C/script%3E

WHY 2 tokens
https://habr.com/ru/company/Voximplant/blog/323160/

More JWT
https://medium.com/@ddarie/jwt-authentication-with-sha-and-rsa-307e272f913f

Japanese OAuth
https://www.youtube.com/watch?v=5zflFpp3AFY&list=PLRUD_uiAYejRvQWkS2xjgFW20lRLp4snN

Helmet:See also
https://helmetjs.github.io/see-also/
